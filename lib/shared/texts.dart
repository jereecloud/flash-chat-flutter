import 'package:flash_chat/shared/languages.dart';

class Texts {
  static var _elanguage = Languages.english;

  static void setLanguage(Languages chosen) {
    _elanguage = chosen;
  }

  static String register() {
    switch (_elanguage) {
      case Languages.english:
        return "Register";
        break;
      case Languages.espanol:
        return "Registrarse";
        break;
      default:
        return "Register";
    }
  }

  static String login() {
    switch (_elanguage) {
      case Languages.english:
        return "Log In";
        break;
      case Languages.espanol:
        return "Ingresar";
        break;
      default:
        return "Log In";
    }
  }

  static String hintDefault() {
    switch (_elanguage) {
      case Languages.english:
        return 'Enter a value';
        break;
      case Languages.espanol:
        return 'Ingrese el valor';
        break;
      default:
        return 'Enter a value';
    }
  }

  static String hintEmail() {
    switch (_elanguage) {
      case Languages.english:
        return 'Enter your value';
        break;
      case Languages.espanol:
        return 'Ingrese su email';
        break;
      default:
        return 'Enter your email';
    }
  }

  static String hintPassword() {
    switch (_elanguage) {
      case Languages.english:
        return 'Enter your password';
        break;
      case Languages.espanol:
        return 'Ingrese su contraseña';
        break;
      default:
        return 'Enter your password';
    }
  }
}
