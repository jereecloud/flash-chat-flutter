import 'package:flutter/material.dart';
import 'screens/screens.dart';
import 'components/components.dart';
import 'shared/shared.dart';

void main() {
  Texts.setLanguage(Languages.english);
  runApp(FlashChat());
}

class FlashChat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        textTheme: TextTheme(
          bodyText2: TextStyle(color: Colors.black54),
        ),
      ),
      home: WelcomeScreen(),
      initialRoute: WelcomeScreen.id,
      routes: {
        'login_screen': (context) => LoginScreen(),
        WelcomeScreen.id: (context) => WelcomeScreen(),
        'registration_screen': (context) => RegistrationScreen(),
        'chat_screen': (context) => ChatScreen(),
      },
    );
  }
}
